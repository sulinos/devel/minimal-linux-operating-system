all: build

build:
	unshare -ru bash tools/build.sh
	unshare -ru bash tools/mkinitrd.sh

rebuild:
	rm -fvr work/*/.build
	make build

reinstall:
	rm -fvr work/*/.install work/rootfs/ work/isowork work/initramfs
	make build
	
clean:
	rm -rf work
