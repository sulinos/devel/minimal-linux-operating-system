# Minimal linux system
## Usage:
Run `make` command

## Writing module
1. create directory in src
1. create build.mk like this
```makefile
fetch:
	wget -c https://source/archive.tar.gz
	tar -xf archive.tar.xz
build:
	./configure --prefix=/
	make -j`nproc`
install:
	make install DESTDIR=$(DESTDIR)
```
