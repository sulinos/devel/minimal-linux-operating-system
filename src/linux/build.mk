#!/usr/bin/make -f
VERSION=5.17.9
fetch:
	wget -c https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-$(VERSION).tar.xz
	tar -xf linux-$(VERSION).tar.xz
	mv linux-$(VERSION)/* .
	wget -c https://raw.githubusercontent.com/archlinux/svntogit-packages/packages/linux/trunk/config -O .config

build:
	cat linux-config > .config
	yes "" | make bzImage modules -j`nproc`

install:
	mkdir -p $(DESTDIR)/lib/modules $(DESTDIR)/boot/
	install arch/x86/boot/bzImage $(DESTDIR)/boot/vmlinuz-$(VERSION)
	install arch/x86/boot/bzImage $(ISOWORK)/linux
	make modules_install INSTALL_MOD_PATH=$(DESTDIR) -j`nproc`
	
